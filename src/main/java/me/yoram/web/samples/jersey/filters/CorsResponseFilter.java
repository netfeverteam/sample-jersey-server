/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.web.samples.jersey.filters;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 23/03/18
 */
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 * Allow the system to serve xhr level 2 from all cross domain site
 *
 * Based on code from OpenESB to test a bug at
 * http://openesb-community-forum.794670.n2.nabble.com/REST-BC-No-Access-Control-Allow-Origin-td7581788.html
 *
 * All original authors has been kept
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 * @author theyoz
 */
@Provider
@PreMatching
public class CorsResponseFilter implements ContainerResponseFilter {
    @Context
    private HttpServletRequest sr;

    /**
     * Add the cross domain data to the output if needed.
     *
     * @param reqCtx The container request (input)
     * @param respCtx The container request (output)
     */
    @Override
    public void filter(final ContainerRequestContext reqCtx, final ContainerResponseContext respCtx) {
        final MultivaluedMap<String, Object> headers = respCtx.getHeaders();

        //headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Origin", sr.getRemoteHost());
        headers.add(
                "Access-Control-Allow-Headers",
                "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
        headers.add("Access-Control-Allow-Credentials", "true");
        headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, X-XSRF-TOKEN");
        headers.add("Access-Control-Max-Age", "1209600");
    }
}