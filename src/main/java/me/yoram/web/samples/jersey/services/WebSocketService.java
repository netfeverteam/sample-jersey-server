/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.web.samples.jersey.services;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.Date;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 23/03/18
 */
@ServerEndpoint("/websocket")
public class WebSocketService {
    private boolean finished = false;

    @OnMessage
    public void onMessage(
            final Session session,
            final String message) {
        if ("finished".equals(message)) {
            finished = true;
        }
    }

    @OnOpen
    public void onOpen(final Session session) {
        new Thread(() -> {
            try {
                while (!finished) {
                    session.getBasicRemote().sendText(new Date().toString());
                    Thread.sleep(2000);
                }

                session.close();
            } catch (Throwable t) {
                t.printStackTrace();
                finished = true;
            }

        }).start();
    }

    @OnClose
    public void onClose(final Session session) {
    }
}
