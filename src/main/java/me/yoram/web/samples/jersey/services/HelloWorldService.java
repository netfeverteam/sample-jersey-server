/* Copyright (C) 2017 Yoram Halberstam.- All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoram Halberstam <yoram.halberstam@gmail.com>
 */
package me.yoram.web.samples.jersey.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 23/03/18
 */
@Path("/helloworld")
public class HelloWorldService {
    private static class Message {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/sayHello")
    public Response sayHello() {
        return Response.ok().entity("Hello World!").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/sayHelloInJson")
    public Response sayHelloJson() {
        final Message message = new Message();
        message.setMessage("Hello World!");
        return Response.ok().entity(message).build();
    }
}
