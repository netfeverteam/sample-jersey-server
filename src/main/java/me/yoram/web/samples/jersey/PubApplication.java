package me.yoram.web.samples.jersey;

import me.yoram.web.samples.jersey.filters.CorsResponseFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.sse.SseFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class PubApplication extends ResourceConfig {
    public PubApplication() {
        packages("me.yoram.web.samples.jersey.services");

        register(CorsResponseFilter.class);
        register(SseFeature.class);

        // if you want J2EE roles
        // register(RolesAllowedDynamicFeature.class);

        // if you want to include Multipart filter
        // register(MultiPartFeature.class);
    }
}