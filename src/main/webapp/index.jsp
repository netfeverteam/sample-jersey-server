<%@ page import="java.net.URL" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
    <head>
        <script>
            var socket;

            function get(url, contenttype) {
                var xhttp = new XMLHttpRequest();
                xhttp.open("GET", url, false);
                xhttp.setRequestHeader("Content-type", contenttype);
                xhttp.send();
                var response = JSON.parse(xhttp.responseText).message;
                alert(response);
            }

            function start(url, id) {
                socket = new WebSocket(url);

                socket.onmessage = function (ev) {
                    var textarea = document.getElementById(id);
                    textarea.value = textarea.value + "\n" + ev.data;
                    textarea.scrollTop = textarea.scrollHeight;
                };

                socket.onclose = function (ev) {
                    document.getElementById("startws").disabled = false;
                    document.getElementById("stopws").disabled = true;
                };

                document.getElementById("startws").disabled = true;
                document.getElementById("stopws").disabled = false;
            }

            function stop() {
                // we could do socket.close() instead
                socket.send("finished");
            }
        </script>
    </head>
<body>
<p>
    <button
            type="submit"
            onclick="get('<c:url value="/services/helloworld/sayHelloInJson"/>', 'application/json')">
        Call Hello World from CORS
    </button>
</p>

<hr/>

<p>
    <table style="width: 100%">
        <td style="width: 80%; margin: 5px">
            <textarea
                    style="width: 100%; height: 200px; overflow-y: scroll; resize: none"
                    id="websocket-messages"></textarea>
        </td>
        <td style="width: 80%; margin: 5px">
            <%
                URL thisUrl = new URL(request.getRequestURL().toString());
                String websocketUrl = new URL(thisUrl, request.getContextPath() + "/websocket").toString().replace(thisUrl.getProtocol() + "://", "ws://");
            %>

            <button
                type="submit"
                onclick="start('<%=websocketUrl%>', 'websocket-messages')"
                id="startws">
                Start
            </button>
            <br>
            <button
                    type="submit"
                    onclick="stop()"
                    id="stopws"
                    disabled="disabled">
                Stop
            </button>
        </td>
    </table>
</p>
</body>
</html>
